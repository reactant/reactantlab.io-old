import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
  links: NavLink[] = [
    {type: 'href', value: 'https://reactant.gitlab.io/cookbook-dev/', name: 'Docs'},
    {type: 'href', value: 'https://gitlab.com/reactant/reactant', name: 'Community'},
    {type: 'routerLink', value: 'contributors', name: 'Contributors'},
    {type: 'href', value: 'https://gitter.im/reactantio/community', name: 'Discussion'}
  ];

  ngOnInit() {
  }

}

type LinkType = 'href' | 'routerLink';

interface NavLink {
  type: LinkType;
  value: string;
  name: string;
}
