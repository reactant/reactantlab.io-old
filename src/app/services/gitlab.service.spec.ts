import {TestBed} from '@angular/core/testing';

import {GitLabService} from './gitlab.service';

describe('GitLabService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GitLabService = TestBed.get(GitLabService);
    expect(service).toBeTruthy();
  });
});
