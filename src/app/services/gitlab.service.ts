import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GitLabService {
  private repositoryId = 12345328;
  private baseUrl = 'https://gitlab.com/api/v4';

  constructor(private http: HttpClient) {
  }

  getContributors(page = 1, sort = 'desc') {
    const url = new URL(`${this.baseUrl}/projects/${this.repositoryId}/repository/contributors`);
    url.searchParams.append('sort', sort);
    url.searchParams.append('page', String(page));
    url.searchParams.append('per_page', String(50));
    return this.http.get<any>(url.href, {observe: 'response'});
  }

  getAvatar(email: string) {
    return this.http.get<any>(`${this.baseUrl}/avatar?email=${email}`).pipe(map((res) => res.avatar_url));
  }
}

export interface Contributor {
  name: string;
  email: string;
  commits: number;
  avatar?: string;
}

