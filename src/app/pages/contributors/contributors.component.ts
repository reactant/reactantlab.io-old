import {Component, OnInit} from '@angular/core';
import {Contributor, GitLabService} from '../../services/gitlab.service';
import {catchError, finalize, flatMap, map, tap} from 'rxjs/operators';
import {of} from 'rxjs';

@Component({
  selector: 'app-contributors',
  templateUrl: './contributors.component.html',
  styleUrls: ['./contributors.component.scss']
})
export class ContributorsComponent implements OnInit {
  requesting = false;
  contributors: Contributor[] = [];

  nextPage = 1;

  constructor(private gitlab: GitLabService) {
  }

  ngOnInit() {
    this.loadContributors();
  }

  loadContributors() {
    if (!this.requesting && this.nextPage != null) {
      this.requesting = true;
      this.gitlab.getContributors(this.nextPage)
        .pipe(
          tap((res) => this.nextPage = parseInt(res.headers.get('X-Next-Page'), 10)),
          flatMap(res => res.body as Contributor[]),
          flatMap((contributor) => {
            return this.gitlab.getAvatar(contributor.email)
              .pipe(
                tap(avatar => contributor.avatar = avatar),
                catchError(() => of('')),
                map(() => contributor)
              );
          }),
          // toArray(),
          finalize(() => this.requesting = false)
        )
        .subscribe((res) => {
          this.contributors.push(res);
        });
    }
  }

  onScrollDown() {
    this.loadContributors();
  }
}
