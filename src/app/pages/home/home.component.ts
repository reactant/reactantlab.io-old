import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  bubblesAmount = 8;
  backBubbles: Bubble[] = [...Array(this.bubblesAmount)].map(() => new Bubble(0.7));
  frontBubbles: Bubble[] = [...Array(this.bubblesAmount)].map(() => new Bubble());


  constructor() {
    console.log(
    );
  }

  ngOnInit() {
  }
}

class Bubble {
  x: number;
  y: number;
  size: number;

  constructor(scale = 1) {
    this.x = Math.random() * 100;
    this.y = Math.random() * 100;
    this.size = (Math.random() * 8 + 2) * scale;
  }

  get style() {
    return {
      width: `${this.size}vw`,
      height: `${this.size}vw`,
      top: `${this.y}%`,
      left: `${this.x}%`,
    };
  }
}
